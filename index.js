const { performance } = require("perf_hooks");
import Board from "./src/Board";
import Obstacle from "./src/Obstacle";
import BoardSolver from "./src/BoardSolver";

function bfsSearch() {
  const visitedBoards = [];

  // An easy board for quick tests during development.
  const boardEasy = [
    ["X", "X", "X", "X", "X", "X"],
    ["X", "a", "b", "b", " ", "X"],
    ["X", "a", "b", "b", " ", "X"],
    ["X", "d", "e", "e", " ", "X"],
    ["X", " ", " ", " ", "m", "X"],
    ["X", " ", " ", " ", "m", "X"],
    ["X", "X", "Z", "Z", "X", "X"]
  ];

  // The actual biard which need to be solved for the quizz.
  const boardHard = [
    ["X", "X", "X", "X", "X", "X"],
    ["X", "a", "b", "b", "c", "X"],
    ["X", "a", "b", "b", "c", "X"],
    ["X", "d", "e", "e", "f", "X"],
    ["X", "d", "g", "h", "f", "X"],
    ["X", "i", " ", " ", "j", "X"],
    ["X", "X", "Z", "Z", "X", "X"]
  ];
  const board = new Board(boardHard, undefined, undefined);
  const boardSolver = new BoardSolver(board);
  const bestSolution = boardSolver.solveForFirstNSolutions(1)[0];

  console.log("*** SOLUTION ***");
  console.log(bestSolution);
}

bfsSearch();
