import MoveDirection from "./MoveDirections";

export default class Move {
  /**
   *
   * @param {string} obstacleName - The to be moved obstacle name
   * @param {MoveDirection} direction - The direction in which to move
   */
  constructor(obstacleName, direction) {
    this.obstacleName = obstacleName;
    this.direction = direction;
  }
}
