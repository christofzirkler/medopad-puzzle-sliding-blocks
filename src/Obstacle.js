export default class Obstacle {
  constructor(name, x, y, height, width) {
    this.name = name;
    this.x = x;
    this.y = y;
    this.height = height;
    this.width = width;
  }

  /**
   * Returns all the cells which are occupied by the Obstacle.
   */
  getAllOccupiedCells() {
    let occupiedCells = [];
    for (var y = this.y; y < this.y + this.height; y++) {
      for (var x = this.x; x < this.x + this.width; x++) {
        occupiedCells.push({ x, y });
      }
    }
    return occupiedCells;
  }
}
