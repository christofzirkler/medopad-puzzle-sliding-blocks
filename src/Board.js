import Obstacle from "./Obstacle";
import Move from "./Move";
import MoveDirections from "./MoveDirections";

/**
 * The board object represents a Board with Obstacles placed on it.
 * Also has a reference to the previous board and to the step which led to this board.
 */
export default class Board {
  constructor(boardStateArray, previousBoard, previousMove) {
    this.state = boardStateArray;
    this.previousBoard = previousBoard;
    this.previousMove = previousMove;
  }

  /**
   * Searches the board for the given obstacle name and  build a obstacle object.
   * @param {String} The name of the requested obstacle.
   * @returns {Obstacle} The corresponding obstacle with x, y, width and height properties.
   */
  getObstacleByName(obstacleName) {
    // Find the coordinates of the given obstacle name

    if (obstacleName === "X" || obstacleName === "Z") {
      console.log("Invalid obstacle Name");
      return;
    }

    var obstacleTopY = Number.MAX_VALUE;
    var obstacleTopX = Number.MAX_VALUE;
    var obstacleBottomX = -1;
    var obstacleBottomY = -1;

    for (var y = 0; y < this.state.length; y++) {
      for (var x = 0; x < this.state[y].length; x++) {
        if (this.state[y][x] === obstacleName) {
          if (y < obstacleTopY) {
            obstacleTopY = y;
          }
          if (x < obstacleTopX) {
            obstacleTopX = x;
          }
          if (y > obstacleBottomY) {
            obstacleBottomY = y;
          }
          if (x > obstacleBottomX) {
            obstacleBottomX = x;
          }
        }
      }
    }

    return new Obstacle(
      obstacleName,
      obstacleTopX,
      obstacleTopY,
      obstacleBottomY - obstacleTopY + 1,
      obstacleBottomX - obstacleTopX + 1
    );
  }

  /**
   * Check if the given move is valid/allowed.
   * @param {Move} The to be validated move.
   * @returns {Boolean} A boolean indicating weather this move is valid or not.
   */
  isMoveValid(move) {
    const obstacle = this.getObstacleByName(move.obstacleName);
    var validMove = true;

    // Check if the cells, which the obstacle would occupy if moved, are empty or part of the obstacle itself
    obstacle.getAllOccupiedCells().forEach(cell => {
      var occupiedCell = "N/A";
      switch (move.direction) {
        case MoveDirections.RIGHT:
          occupiedCell = this.state[cell.y + 0][cell.x + 1];
          break;
        case MoveDirections.LEFT:
          occupiedCell = this.state[cell.y + 0][cell.x - 1];
          break;
        case MoveDirections.UP:
          occupiedCell = this.state[cell.y - 1][cell.x + 0];
          break;
        case MoveDirections.DOWN:
          occupiedCell = this.state[cell.y + 1][cell.x + 0];
          break;
      }

      // Check if cell is already occupied by another cell
      if (occupiedCell !== " " && occupiedCell !== move.obstacleName) {
        validMove = false;
      }
    });
    return validMove;
  }

  /**
   * Returns the names of all obstacles on the board.
   * @return {Array} An array containing the names of all obstacles.
   */
  getAllObstacleNamesOnBoard() {
    var obstacles = [];
    for (var y = 1; y < this.state.length - 1; y++) {
      for (var x = 1; x < this.state[y].length - 1; x++) {
        let currentobstacleName = this.state[y][x];
        if (currentobstacleName === " ") {
          continue;
        }
        if (!obstacles.includes(currentobstacleName)) {
          obstacles.push(currentobstacleName);
        }
      }
    }
    return obstacles;
  }

  /**
   * Ruturns all moves which are possible for the current state.
   * @returns {Array} All possible moves.
   */
  getAllPossibleMoves() {
    var availableMoves = [];
    let allobstacles = this.getAllObstacleNamesOnBoard();
    allobstacles.forEach(obstacle => {
      [
        MoveDirections.RIGHT,
        MoveDirections.LEFT,
        MoveDirections.UP,
        MoveDirections.DOWN
      ].forEach(moveDirection => {
        let move = new Move(obstacle, moveDirection);
        if (this.isMoveValid(move)) {
          availableMoves.push(move);
        }
      });
    });
    return availableMoves;
  }

  /**
   * Performs the given move, then returns a new board state on which the move is applied.
   * Does not manipulate the current board.
   * @param {string} obstacleName The to be moved obstacle.
   * @param {string} direction The direction in which to move the given obstacle.
   * @returns {Array} The resulting board state.
   */
  performMove(move) {
    if (!this.isMoveValid(move.obstacleName, move.direction)) {
      console.log("Tried to perform an invalid move.");
    }

    // Deep copy the state
    var newBoardState = JSON.parse(JSON.stringify(this.state));
    const obstacle = this.getObstacleByName(move.obstacleName);

    // Perform the move
    let originCells = obstacle.getAllOccupiedCells();

    // Clear the previous cells
    originCells.forEach(cell => {
      newBoardState[cell.y][cell.x] = " ";
    });

    // Then insert the obstacle at the new (moved) position
    originCells.forEach(cell => {
      switch (move.direction) {
        case MoveDirections.RIGHT:
          newBoardState[cell.y][cell.x + 1] = move.obstacleName;
          break;
        case MoveDirections.LEFT:
          newBoardState[cell.y][cell.x - 1] = move.obstacleName;
          break;
        case MoveDirections.UP:
          newBoardState[cell.y - 1][cell.x] = move.obstacleName;
          break;
        case MoveDirections.DOWN:
          newBoardState[cell.y + 1][cell.x] = move.obstacleName;
          break;
      }
    });
    return newBoardState;
  }

  /**
   * @returns {String} Compact string Representation of the current board.
   */
  getStringRepresentation() {
    let output = "";
    for (var y = 0; y < this.state.length; y++) {
      for (var x = 0; x < this.state[y].length; x++) {
        output += this.state[y][x];
      }
      output += "\n";
    }
    return output;
  }

  /**
   * @returns {String} Hash of the board state.
   */
  hash() {
    let str = this.getStringRepresentation();
    var hash = 0,
      i,
      chr,
      len;
    if (str.length === 0) return hash;
    for (i = 0, len = str.length; i < len; i++) {
      chr = str.charCodeAt(i);
      hash = (hash << 5) - hash + chr;
      hash |= 0; // Convert to 32bit integer
    }
    return hash;
  }

  /**
   * Walks back on all previous board and returns an array of moves which led to this board.
   * @returns {Array} All the  Move objects which led to this board.
   */
  getPrecedingMoves() {
    let precedingMoves = [this.previousMove];
    var b = this.previousBoard;
    while (b.previousBoard) {
      precedingMoves.push(b.previousMove);
      b = b.previousBoard;
    }
    return precedingMoves.reverse();
  }
}
module.exports.Board = Board;
