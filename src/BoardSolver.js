import Board from "./Board";
import Obstacle from "./Obstacle";
import Move from "./Move";
import MoveDirections from "./MoveDirections";
var RBTree = require("bintrees").RBTree;

/**
 * BoardSolver performs a breadth first search to find solutions for the given initialBoard.
 */
export default class BoardSolver {
  constructor(initialBoard) {
    this.initialBoard = initialBoard;

    // With help of the bfsQueue we perform the breadt first search.
    this.bfsQueue = [initialBoard];

    // We store the already visitied board in an AVL tree so we can perform very fast lookups.
    this.visitedBoardsTree = new RBTree(function(a, b) {
      return a - b;
    });

    this.amountOfSolution = 100;
    this.foundSolutions = [];
  }

  /**
   * Solves the Board and returns all the moves which are  required to  solve the board.
   */
  solveForFirstNSolutions(amountOfSolutions) {
    this.amountOfSolution = amountOfSolutions;
    while (this.bfsQueue.length !== 0) {
      let currentBoard = this.bfsQueue.shift();

      // Check if this is a winning state
      if (
        currentBoard.getObstacleByName("b").y === 4 &&
        currentBoard.getObstacleByName("b").x === 2
      ) {
        // We found a solution
        this.visitedBoardsTree.insert(currentBoard.hash());
        this.foundSolutions.push(currentBoard.getPrecedingMoves());

        if (this.foundSolutions.length >= this.amountOfSolution) {
          // Return the moves of all found solutions.
          return this.foundSolutions;
        }
      }

      currentBoard.getAllPossibleMoves().forEach(move => {
        // Get the descendant board by performing an move.
        let resultingBoardState = currentBoard.performMove(move);
        let branchedBoard = new Board(resultingBoardState, currentBoard, move);
        let boardHash = branchedBoard.hash();
        const boardAlreadyVisited =
          this.visitedBoardsTree.find(boardHash) !== null;

        // Only print debug info when the tree grew another 100.000 items, printing to much slows down the solving process.
        if (this.visitedBoardsTree.size % (100 * 1000) === 0) {
          console.log("bfsQueue.length", this.bfsQueue.length);
          console.log("tree.size", this.visitedBoardsTree.size);
        }

        // If we haven't visited that board alrady, add it to the bfs queue so we will check it's descendants on a later iteration.
        if (!boardAlreadyVisited) {
          this.visitedBoardsTree.insert(boardHash);
          this.bfsQueue.push(branchedBoard);
        }
      });
    }
  }
}
