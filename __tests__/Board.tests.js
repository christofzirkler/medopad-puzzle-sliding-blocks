import Board from "../src/Board";
import Move from "../src/Move";
import MoveDirections from "../src/MoveDirections";

test("Board.getAllObstacleNamesOnBoard()", () => {
  const initialBoard = [
    ["X", "X", "X", "X", "X", "X"],
    ["X", "a", "b", "b", "c", "X"],
    ["X", "a", "b", "b", "c", "X"],
    ["X", "d", "e", "e", "f", "X"],
    ["X", "d", "g", "h", "f", "X"],
    ["X", "i", " ", " ", "j", "X"],
    ["X", "X", "Z", "Z", "X", "X"]
  ];
  let board = new Board(initialBoard);
  let obstacles = board.getAllObstacleNamesOnBoard();
  expect(obstacles).toEqual("a,b,c,d,e,f,g,h,i,j".split(","));
});

test("Board valid moves for single digit obstacles", () => {
  const initialBoard = [
    ["X", "X", "X", "X", "X", "X"],
    ["X", "a", "b", "b", "c", "X"],
    ["X", "a", "b", "b", "c", "X"],
    ["X", "d", "e", "e", "f", "X"],
    ["X", "d", "g", "h", "f", "X"],
    ["X", "i", " ", " ", "j", "X"],
    ["X", "X", "Z", "Z", "X", "X"]
  ];
  let board = new Board(initialBoard);

  expect(board.isMoveValid(new Move("i", MoveDirections.RIGHT))).toBe(true);
  expect(board.isMoveValid(new Move("i", MoveDirections.LEFT))).toBe(false);
  expect(board.isMoveValid(new Move("g", MoveDirections.DOWN))).toBe(true);
  expect(board.isMoveValid(new Move("h", MoveDirections.DOWN))).toBe(true);
  expect(board.isMoveValid(new Move("i", MoveDirections.UP))).toBe(false);
});

test("Board valid moves for 2x3 obstacles", () => {
  let board2 = new Board([
    ["X", "X", "X", "X", "X", "X"],
    ["X", "a", "b", "b", "c", "X"],
    ["X", "a", "b", "b", "c", "X"],
    ["X", "d", "b", "b", "f", "X"],
    ["X", "d", " ", " ", "f", "X"],
    ["X", "i", " ", " ", "j", "X"],
    ["X", "X", "Z", "Z", "X", "X"]
  ]);

  expect(board2.isMoveValid(new Move("b", MoveDirections.DOWN))).toBe(true);
  expect(board2.isMoveValid(new Move("b", MoveDirections.RIGHT))).toBe(false);
  expect(board2.isMoveValid(new Move("b", MoveDirections.LEFT))).toBe(false);
  expect(board2.isMoveValid(new Move("b", MoveDirections.UP))).toBe(false);
});

test("Board valid moves for 2x2 obstacle (all directions free)", () => {
  let board2 = new Board([
    ["X", "X", "X", "X", "X", "X"],
    ["X", " ", " ", " ", " ", "X"],
    ["X", " ", "b", "b", " ", "X"],
    ["X", " ", "b", "b", " ", "X"],
    ["X", " ", " ", " ", " ", "X"],
    ["X", " ", " ", " ", " ", "X"],
    ["X", "X", "Z", "Z", "X", "X"]
  ]);

  expect(board2.isMoveValid(new Move("b", MoveDirections.DOWN))).toBe(true);
  expect(board2.isMoveValid(new Move("b", MoveDirections.RIGHT))).toBe(true);
  expect(board2.isMoveValid(new Move("b", MoveDirections.LEFT))).toBe(true);
  expect(board2.isMoveValid(new Move("b", MoveDirections.UP))).toBe(true);
});

test("Board valid moves for 2x2 obstacle with on obstacle", () => {
  let board2 = new Board([
    ["X", "X", "X", "X", "X", "X"],
    ["X", " ", " ", " ", " ", "X"],
    ["X", " ", "b", "b", "a", "X"],
    ["X", " ", "b", "b", " ", "X"],
    ["X", " ", " ", " ", " ", "X"],
    ["X", " ", " ", " ", " ", "X"],
    ["X", "X", "Z", "Z", "X", "X"]
  ]);

  expect(board2.isMoveValid(new Move("b", MoveDirections.DOWN))).toBe(true);
  expect(board2.isMoveValid(new Move("b", MoveDirections.RIGHT))).toBe(false);
  expect(board2.isMoveValid(new Move("b", MoveDirections.LEFT))).toBe(true);
  expect(board2.isMoveValid(new Move("b", MoveDirections.UP))).toBe(true);
});

test("performMove", () => {
  let board = new Board([
    ["X", " ", "X", "X", "X", "X"],
    ["X", " ", " ", " ", " ", "X"],
    ["X", " ", "b", "b", " ", "X"],
    ["X", " ", "b", "b", " ", "X"],
    ["X", " ", " ", " ", " ", "X"],
    ["X", " ", " ", " ", " ", "X"],
    ["X", " ", "Z", "Z", "X", "X"]
  ]);

  let expectedBoardStateAfteerMoveRight = [
    ["X", " ", "X", "X", "X", "X"],
    ["X", " ", " ", " ", " ", "X"],
    ["X", " ", " ", "b", "b", "X"],
    ["X", " ", " ", "b", "b", "X"],
    ["X", " ", " ", " ", " ", "X"],
    ["X", " ", " ", " ", " ", "X"],
    ["X", " ", "Z", "Z", "X", "X"]
  ];

  let expectedBoardStateAfterMoveLeft = [
    ["X", " ", "X", "X", "X", "X"],
    ["X", " ", " ", " ", " ", "X"],
    ["X", "b", "b", " ", " ", "X"],
    ["X", "b", "b", " ", " ", "X"],
    ["X", " ", " ", " ", " ", "X"],
    ["X", " ", " ", " ", " ", "X"],
    ["X", " ", "Z", "Z", "X", "X"]
  ];

  let expectedBoardStateAfterMoveUp = [
    ["X", " ", "X", "X", "X", "X"],
    ["X", " ", "b", "b", " ", "X"],
    ["X", " ", "b", "b", " ", "X"],
    ["X", " ", " ", " ", " ", "X"],
    ["X", " ", " ", " ", " ", "X"],
    ["X", " ", " ", " ", " ", "X"],
    ["X", " ", "Z", "Z", "X", "X"]
  ];

  let expectedBoardStateAfterMoveDown = [
    ["X", " ", "X", "X", "X", "X"],
    ["X", " ", " ", " ", " ", "X"],
    ["X", " ", " ", " ", " ", "X"],
    ["X", " ", "b", "b", " ", "X"],
    ["X", " ", "b", "b", " ", "X"],
    ["X", " ", " ", " ", " ", "X"],
    ["X", " ", "Z", "Z", "X", "X"]
  ];

  let newBoardRight = board.performMove(new Move("b", MoveDirections.RIGHT));
  expect(newBoardRight).toEqual(expectedBoardStateAfteerMoveRight);
  const newBoardLeft = board.performMove(new Move("b", MoveDirections.LEFT));
  expect(newBoardLeft).toEqual(expectedBoardStateAfterMoveLeft);
  const newBoardUp = board.performMove(new Move("b", MoveDirections.UP));
  expect(newBoardUp).toEqual(expectedBoardStateAfterMoveUp);
  const newBoardDown = board.performMove(new Move("b", MoveDirections.DOWN));
  expect(newBoardDown).toEqual(expectedBoardStateAfterMoveDown);
});

test("Board.getAllPossibleMoves()", () => {
  let board2 = new Board([
    ["X", "X", "X", "X", "X", "X"],
    ["X", " ", " ", " ", " ", "X"],
    ["X", " ", "b", "b", "a", "X"],
    ["X", " ", "b", "b", " ", "X"],
    ["X", " ", " ", " ", " ", "X"],
    ["X", " ", " ", " ", " ", "X"],
    ["X", "X", "Z", "Z", "X", "X"]
  ]);

  let moves = board2.getAllPossibleMoves();
  expect(moves).toMatchSnapshot();
});

test("Board.hash()", () => {
  let board = new Board([
    ["X", "X", "X", "X", "X", "X"],
    ["X", " ", " ", " ", " ", "X"],
    ["X", " ", "b", "b", "a", "X"],
    ["X", " ", "b", "b", " ", "X"],
    ["X", " ", " ", " ", " ", "X"],
    ["X", " ", " ", " ", " ", "X"],
    ["X", "X", "Z", "Z", "X", "X"]
  ]);
  expect(board.hash()).toBe(943962891);
});
