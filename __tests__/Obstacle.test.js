import Board from "../src/Board";
import Obstacle from "../src/Obstacle";

test("Obstacle should correctly return occupied coordinates", () => {
  const initialBoard = [
    ["X", "X", "X", "X", "X", "X"],
    ["X", "a", "b", "b", "c", "X"],
    ["X", "a", "b", "b", "c", "X"],
    ["X", "d", "e", "e", "f", "X"],
    ["X", "d", "g", "h", "f", "X"],
    ["X", "i", " ", " ", "j", "X"],
    ["X", "X", "Z", "Z", "X", "X"]
  ];

  let board = new Board(initialBoard);
  let bObstacle = board.getObstacleByName("b");
  let dObstacle = board.getObstacleByName("d");
  let occupiedCells = expect(bObstacle.getAllOccupiedCells()).toMatchSnapshot();
  expect(dObstacle.getAllOccupiedCells()).toMatchSnapshot();
});
