This project is a drop-in solver for the "medopad sliding block problem".

## The Problem:

To solve the puzzle, you must move the pieces around the board and move b out of the exit Z.

The rules are:

- A piece may only move vertically or horizontally into empty space next to it. (example moves are illustrated below)
- Pieces may not occupy the space Z.
- The piece b may move through Z solving the problem.
- A piece can not overlap with another piece.
- There are always 2 empty spaces on the board.
- The following is a visualization of the pieces with the board in the initial state :

```
XXXXXX
XabbcX
XabbcX
XdeefX
XdghfX
Xi  jX
XXZZXX
```

X is a border, and Z is a gap through which the main piece b can move. We consider the problem solved once the b component is positioned directly above the ZZ position.

## Requirements

- Node >= v10

## Installation

- Clone this Repository
- run `$ npm install`

## Running the Tests

During the development process I have written a few tests which helped me to ensure core functionality is working correctly. The tests can be executed by running `$ npm test`

## Usage of the solver

For solving the problem board provided in the assessment task simply run `$ npm start`. The ProblemSolver module can be used as drop in solution for problems of this kind. The ProblemSolver can be used like the following

```
  const initialBoardState = [
    ["X", "X", "X", "X", "X", "X"],
    ["X", "a", "b", "b", "c", "X"],
    ["X", "a", "b", "b", "c", "X"],
    ["X", "d", "e", "e", "f", "X"],
    ["X", "d", "g", "h", "f", "X"],
    ["X", "i", " ", " ", "j", "X"],
    ["X", "X", "Z", "Z", "X", "X"]
  ];
  const board = new Board(initialBoardState, undefined, undefined);
  const boardSolver = new BoardSolver(board);
  const bestSolution = boardSolver.solveForFirstNSolutions(1)[0];

  console.log("*** SOLUTION ***");
  console.log(bestSolution);
```

This will output something like the following:

```
[ Move { obstacleName: 'b', direction: 'RIGHT' },
  Move { obstacleName: 'a', direction: 'RIGHT' },
  ...
  Move { obstacleName: 'm', direction: 'LEFT' },
  Move { obstacleName: 'b', direction: 'LEFT' } ]
```

From top to bottom these are the moves required to get from the initial board state to a solution board state.

## Algorithm

Basically we perform a breadth first search over all possible moves and simply watch out for a board where b is directly above ZZ (the "solved state"). On a high level description we do the following:

1. Initialize a queue with the initial board.
2. Pick the first element/board from the queue.
3. Check if this board is in the "solved state".
   1. If yes, we found a solution, done.
4. Get all possible moves for the picked board.
5. Perform all the possible moves and store their resulting boards at the end of the queue.
6. Go to 2.

Since we are performing

## Future Improvements

- Dynamically detect the ZZ exit aka the target position of b.
- Building an GUI for visualizing the moves on the board.
